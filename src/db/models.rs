use super::schema::*;

#[derive(Identifiable, Insertable, Queryable, AsChangeset, Debug)]
#[table_name = "users"]
#[primary_key(id)]
pub struct User {
    pub id: i64,
    pub yen: i64,
    pub experience: i64,
}

#[derive(Identifiable, Insertable, Queryable, AsChangeset, Debug)]
#[table_name = "servers"]
#[primary_key(id)]
pub struct ServerSettings {
    pub id: i64,
    pub welcome_channel: i64,
    pub voice_channel: i64,
}

#[derive(Identifiable, Insertable, Queryable, AsChangeset, Debug)]
#[table_name = "servers"]
#[primary_key(id)]
pub struct Server {
    pub id: i64,
    pub welcome_channel: i64,
    pub voice_channel: i64,
    pub hangman_words: String,
}

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser {
    pub id: i64,
}

#[derive(Insertable)]
#[table_name="servers"]
pub struct NewServer {
    pub id: i64,
}
