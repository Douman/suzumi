table! {
    users (id) {
        id -> BigInt,
        yen -> BigInt,
        experience -> BigInt,
    }
}

table! {
    servers (id) {
        id -> BigInt,
        welcome_channel -> BigInt,
        voice_channel -> BigInt,
        hangman_words -> Text,
    }
}
