//From to Cargo.toml
embed_migrations!("./migrations/");

mod schema;
pub mod models;

const PATH: &'static str = "./suzumi.db";

use std::sync;

use diesel::sqlite::SqliteConnection;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use diesel::{ExpressionMethods, RunQueryDsl, QueryDsl};

lazy_static! {
    pub static ref DB: Db = Db::default();
}

pub struct Db {
    //Does connection pool even make sense for sqlite?
    inner: Pool<ConnectionManager<SqliteConnection>>,
    lock: sync::RwLock<()>,
}

impl Db {
    fn conn(&self) -> PooledConnection<ConnectionManager<SqliteConnection>> {
        self.inner.clone().get().expect("Attempt to get connection timed out")
    }

    ///Provides read lock access to connection.
    ///
    ///Should be used for reads
    fn with_conn<R, F: FnOnce(PooledConnection<ConnectionManager<SqliteConnection>>) -> R>(&self, cb: F) -> R {
        let _lock = self.lock.read();

        cb(self.conn())
    }

    ///Provides write lock access to connectios
    ///
    ///Should be used for writes
    fn with_write_conn<R, F: FnOnce(PooledConnection<ConnectionManager<SqliteConnection>>) -> R>(&self, cb: F) -> R {
        //sqlite returns lock error on concurrent writes, so just don't try
        let _lock = self.lock.write();

        cb(self.conn())
    }

    //Server
    pub fn new_server(&self, id: i64) -> diesel::QueryResult<bool> {
        if self.get_server(id).is_ok() {
            return Ok(false);
        }

        let server = models::NewServer {
            id,
        };


        self.with_write_conn(|conn| diesel::insert_into(schema::servers::table).values(&server)
                                                                               .execute(&conn)
                                                                               .map(|_| true))
    }

    pub fn get_server(&self, id: i64) -> diesel::QueryResult<models::Server> {
        self.with_conn(|conn| schema::servers::table.find(id).first(&conn))
    }

    pub fn get_server_settings(&self, server_id: i64) -> diesel::QueryResult<models::ServerSettings> {
        use db::schema::servers::columns::{id, welcome_channel, voice_channel};
        self.with_conn(|conn| schema::servers::table.select((id, welcome_channel, voice_channel)).find(server_id).first(&conn))
    }

    pub fn get_server_hangman_words(&self, id: i64) -> diesel::QueryResult<String> {
        use db::schema::servers::columns::{hangman_words};
        self.with_conn(|conn| schema::servers::table.select(hangman_words).find(id).first(&conn))
    }

    pub fn update_server(&self, server: models::Server) -> diesel::QueryResult<models::Server> {
        self.with_write_conn(|conn| diesel::update(&server).set(&server).execute(&conn).map(|_| server))
    }

    pub fn update_server_settings(&self, server: models::ServerSettings) -> diesel::QueryResult<models::ServerSettings> {
        self.with_write_conn(|conn| diesel::update(&server).set(&server).execute(&conn).map(|_| server))
    }

    pub fn remove_server(&self, server_id: i64) -> diesel::QueryResult<()> {
        use db::schema::servers::columns::id;
        self.with_write_conn(|conn| diesel::delete(schema::servers::table).filter(id.eq(&server_id)).execute(&conn)).map(|_| ())
    }

    //User
    pub fn new_user(&self, id: i64) -> diesel::QueryResult<models::User> {
        let user = models::NewUser {
           id,
        };

        self.with_write_conn(|conn| diesel::insert_into(schema::users::table).values(&user)
                                                                             .execute(&conn))?;

        self.get_user(id)
    }

    pub fn get_user(&self, id: i64) -> diesel::QueryResult<models::User> {
        self.with_conn(|conn| schema::users::table.find(id).first(&conn))
    }

    pub fn get_or_new_user(&self, id: i64) -> diesel::QueryResult<models::User> {
        self.get_user(id).or_else(|_| self.new_user(id))
    }

    pub fn update_user(&self, user: models::User) -> diesel::QueryResult<models::User> {
        self.with_write_conn(|conn| diesel::update(&user).set(&user).execute(&conn).map(|_| user))
    }
}

impl Default for Db {
    fn default() -> Self {
        let manager = ConnectionManager::<SqliteConnection>::new(PATH);
        let inner = Pool::builder().max_size(10)
                                   .build(manager)
                                   .expect("Cannot create database");

        let result = Self {
            inner,
            lock: sync::RwLock::new(()),
        };

        embedded_migrations::run(&result.conn()).expect("To run migrations");

        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_server_settings() {
        let server_id = 500;
        let _ = DB.new_server(server_id).expect("To create new server");

        let mut settings = DB.get_server_settings(server_id).expect("To get settings");
        settings.welcome_channel = 600;
        DB.update_server_settings(settings).expect("To update settings");

        let mut settings = DB.get_server_settings(server_id).expect("To get settings");
        assert_eq!(settings.welcome_channel, 600);
        settings.welcome_channel = 700;

        DB.update_server_settings(settings).expect("To update settings");
        let settings = DB.get_server_settings(server_id).expect("To get settings");
        assert_eq!(settings.welcome_channel, 700);
    }
}
