pub fn is_eng(text: &str) -> bool {
    //TODO: Allow english as language or latin script in general?
    //whatlang::detect(text).map(|info| info.lang() == whatlang::Lang::Eng).unwrap_or(false)
    whatlang::detect(text).map(|info| info.script() == whatlang::Script::Latin).unwrap_or(false)
}
