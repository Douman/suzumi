use std::collections::{hash_map, HashMap};

pub enum GuessResult {
    GameOver(String),
    Victory(String),
    ///Not finished yet
    ///
    ///# Args
    ///
    ///- Current word;
    ///- Left attempts;
    ///- Did you guessed some letter?;
    Mada(String, usize, bool),
}

impl GuessResult {
    fn is_over(&self) -> bool {
        match self {
            GuessResult::GameOver(_) | GuessResult::Victory(_) => true,
            _ => false,
        }
    }
}

struct Game {
    word: String,
    current: String,
    attempts: usize,
    max: usize,
}

impl Game {
    pub fn new(word: String, max: usize) -> Self {
        let mut current = String::with_capacity(word.len());
        for _ in 0..word.len() {
            current.push('_');
        }

        Self {
            word,
            current,
            attempts: 0,
            max,
        }
    }

    pub fn current_word(&self) -> String {
        let mut current = String::with_capacity(self.current.len() * 2);
        for c in self.current.chars() {
            current.push(c);
            current.push(' ');
        }

        current
    }

    pub fn is_game_solved(&self) -> bool {
        self.current == self.word
    }

    pub fn guess_letter(&mut self, letter: char) -> GuessResult {
        debug_assert_eq!(self.word.len(), self.current.len());
        let mut result = false;

        for (idx, ch) in self.word.chars().enumerate() {
            if ch == letter {
                self.current.remove(idx);
                self.current.insert(idx, letter);
                result = true;
            }
        }

        self.attempts += 1;

        if self.is_game_solved() {
            GuessResult::Victory(self.word.clone())
        } else if self.attempts < self.max {
            GuessResult::Mada(self.current_word(), self.max - self.attempts, result)
        } else {
            GuessResult::GameOver(self.current_word())
        }
    }
}

#[derive(Default)]
pub struct Manager {
    ongoing: HashMap<u64, Game>,
}

impl actix::Actor for Manager {
    type Context = actix::Context<Self>;
}

impl actix::Supervised for Manager {
    fn restarting(&mut self, _: &mut actix::Context<Self>) {
        warn!("Play list manager restarting!");
    }
}

///Requests to start New Game
///
///Returns None if game is already running on server.
///
///# Args:
///
///- Server id;
///- Word;
///- Attempts Number;
pub struct NewGame(pub u64, pub String, pub usize);
impl actix::Message for NewGame {
    type Result = Option<String>;
}
impl actix::Handler<NewGame> for Manager {
    type Result = Option<String>;

    fn handle(&mut self, msg: NewGame, _: &mut Self::Context) -> Self::Result {
        match self.ongoing.entry(msg.0) {
            hash_map::Entry::Occupied(_) => None,
            hash_map::Entry::Vacant(entry) => {
                let game = Game::new(msg.1, msg.2);
                let res = game.current_word();
                entry.insert(game);
                Some(res)
            }
        }
    }
}

pub struct GuessLetter(pub u64, pub char);
impl actix::Message for GuessLetter {
    type Result = Option<GuessResult>;
}
impl actix::Handler<GuessLetter> for Manager {
    type Result = Option<GuessResult>;

    fn handle(&mut self, msg: GuessLetter, _: &mut Self::Context) -> Self::Result {
        match self.ongoing.entry(msg.0) {
            hash_map::Entry::Vacant(_) => None,
            hash_map::Entry::Occupied(mut entry) => {
                let result = entry.get_mut().guess_letter(msg.1);

                if result.is_over() {
                    entry.remove();
                }

                Some(result)
            },
        }
    }
}
