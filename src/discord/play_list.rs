use serenity::prelude::{Mutex};
use serenity::client::bridge::voice::ClientVoiceManager;
use actix::prelude::AsyncContext;

use std::time;
use std::sync;
use std::collections::{hash_map, HashMap, VecDeque};

const CHECK_INTERVAL: time::Duration = time::Duration::from_secs(10);

#[derive(Default)]
struct PlayList {
    current: Option<serenity::voice::LockedAudio>,
    queue: VecDeque<Box<dyn serenity::voice::AudioSource>>
}

#[derive(Default)]
pub struct Manager {
    inner: Option<sync::Arc<Mutex<ClientVoiceManager>>>,
    lists: HashMap<u64, PlayList>,
}

impl Manager {
    fn cancel_current(server: u64, inner: &mut Option<&mut sync::Arc<Mutex<ClientVoiceManager>>>, play_list: &mut PlayList) {
        if let Some(inner) = inner.as_mut() {
            if let Some(_) = play_list.current.take() {
                let mut manager = inner.lock();
                match manager.get_mut(server) {
                    Some(handler) => handler.stop(),
                    None => (),
                }
            }
        }
    }

    fn check_loop(&mut self, _: &mut actix::Context<Self>) {
        let inner = match self.inner.as_mut() {
            Some(inner) => inner,
            None => return,
        };

        for (server, play_list) in self.lists.iter_mut() {
            match play_list.current.as_ref() {
                Some(audio) => match audio.lock().finished {
                    true => (),
                    false => return,
                },
                None => (),
            }

            let next = match play_list.queue.pop_front() {
                Some(next) => next,
                None => return,
            };

            let mut manager = inner.lock();

            let locked_audio = match manager.get_mut(*server) {
                Some(handler) => handler.play_returning(next),
                None => {
                    play_list.queue.clear();
                    warn!("Trying to play while not in voice channel");
                    return;
                },
            };

            play_list.current = Some(locked_audio);
        }
    }
}

impl actix::Actor for Manager {
    type Context = actix::Context<Self>;

    fn started(&mut self, ctx: &mut actix::Context<Self>) {
        ctx.run_interval(CHECK_INTERVAL, Self::check_loop);
    }
}

impl actix::Supervised for Manager {
    fn restarting(&mut self, _: &mut actix::Context<Self>) {
        warn!("Play list manager restarting!");
    }
}

pub struct SetVoiceManager(pub sync::Arc<Mutex<ClientVoiceManager>>);
impl actix::Message for SetVoiceManager {
    type Result = ();
}

impl actix::Handler<SetVoiceManager> for Manager {
    type Result = ();

    fn handle(&mut self, msg: SetVoiceManager, _: &mut Self::Context) -> Self::Result {
        info!("Adding voice manager");
        self.inner = Some(msg.0);
    }
}

pub struct UnsetVoiceManager;
impl actix::Message for UnsetVoiceManager {
    type Result = ();
}

impl actix::Handler<UnsetVoiceManager> for Manager {
    type Result = ();

    fn handle(&mut self, _: UnsetVoiceManager, _: &mut Self::Context) -> Self::Result {
        info!("Removing old voice manager");
        self.inner.take();
    }
}

///Requests to add new server to Manager
pub struct AddServer(pub u64);
impl actix::Message for AddServer {
    type Result = ();
}

impl actix::Handler<AddServer> for Manager {
    type Result = ();

    fn handle(&mut self, msg: AddServer, _: &mut Self::Context) -> Self::Result {
        match self.lists.entry(msg.0) {
            hash_map::Entry::Occupied(mut old) => {
                let old = old.get_mut();
                old.queue.clear();

                if let Some(inner) = self.inner.as_mut() {
                    if let Some(_) = old.current.take() {
                        let mut manager = inner.lock();
                        match manager.get_mut(msg.0) {
                            Some(handler) => handler.stop(),
                            None => (),
                        }
                    }
                }
            },
            hash_map::Entry::Vacant(new) => {
                new.insert(PlayList::default());
            },
        }
    }
}

///Requests to remove server to Manager
pub struct RemoveServer(pub u64);
impl actix::Message for RemoveServer {
    type Result = ();
}

impl actix::Handler<RemoveServer> for Manager {
    type Result = ();

    fn handle(&mut self, msg: RemoveServer, _: &mut Self::Context) -> Self::Result {
        match self.lists.entry(msg.0) {
            hash_map::Entry::Occupied(old) => {
                let mut old = old.remove();
                Self::cancel_current(msg.0, &mut self.inner.as_mut(), &mut old);
            },
            _ => (),
        }
    }
}

///Requests to stop current song on server to Manager
pub struct StopCurrent(pub u64);
impl actix::Message for StopCurrent {
    type Result = ();
}

impl actix::Handler<StopCurrent> for Manager {
    type Result = ();

    fn handle(&mut self, msg: StopCurrent, _: &mut Self::Context) -> Self::Result {
        match self.lists.entry(msg.0) {
            hash_map::Entry::Occupied(mut play_list) => {
                Self::cancel_current(msg.0, &mut self.inner.as_mut(), play_list.get_mut());
            },
            _ => (),
        }

    }
}

///Requests to add new song on server to Manager
pub struct AddSong(pub u64, pub Box<dyn serenity::voice::AudioSource>);
impl actix::Message for AddSong {
    type Result = ();
}

impl actix::Handler<AddSong> for Manager {
    type Result = ();

    fn handle(&mut self, msg: AddSong, _: &mut Self::Context) -> Self::Result {
        match self.lists.entry(msg.0) {
            hash_map::Entry::Occupied(mut play_list) => {
                let play_list = play_list.get_mut();

                if let Some(inner) = self.inner.as_mut() {
                    if play_list.current.is_some() {
                        play_list.queue.push_back(msg.1);
                    } else {
                        let mut manager = inner.lock();
                        match manager.get_mut(msg.0) {
                            Some(handler) => {
                                play_list.current = Some(handler.play_returning(msg.1))
                            }
                            None => (),
                        }
                    }
                }
            },
            _ => (),
        }

    }
}
