use serenity::model::channel::Message;
use serenity::framework::standard::{Args, CommandError};
use serenity::prelude::{Mentionable, Context};

use discord::key::VoiceManager;
use db::DB;
use lang;
use constants;

pub const SET_WELCOME_DESC: &'static str = "Sets channel for welcome message";
pub const SET_WELCOME_USAGE: &'static str = "";
pub fn set_welcome_channel(_: &mut Context, message: &Message, _: Args) -> Result<(), CommandError> {
    if let Some(guild_id) = message.guild_id {
        match DB.get_server_settings(guild_id.0 as i64) {
            Ok(mut server) => {
                let channel_id = message.channel_id.0 as i64;

                if server.welcome_channel == channel_id {
                    message.reply("This channel is already set")?;
                } else {
                    server.welcome_channel = channel_id;
                    match DB.update_server_settings(server) {
                        Ok(_) => {
                            message.reply("Set this channel as welcoming one")?;
                        },
                        Err(error) => {
                            warn!("Unable to update server, error: {}", error);
                            message.reply("Cannot update server :(")?;
                        }
                    }
                }
            },
            Err(error) => {
                warn!("Unable to get server, error: {}", error);
                message.reply("You lost your wallet :(")?;
            }
        }
    }

    Ok(())
}

pub const VOICE_ON_DESC: &'static str = "Tells bot to enter the same voice channel as you're";
pub const VOICE_ON_USAGE: &'static str = "";
pub fn voice_on(ctx: &mut Context, msg: &Message, _: Args) -> Result<(), CommandError> {
    let guild = match msg.guild() {
        Some(guild) => guild,
        None => {
            msg.reply("Groups and DMs not supported")?;

            return Ok(());
        }
    };

    let guild_id = guild.read().id;
    let channel_id = guild.read()
                          .voice_states.get(&msg.author.id)
                          .and_then(|voice_state| voice_state.channel_id);

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            msg.reply("You're not in any voice chat, where should I join?")?;

            return Ok(());
        }
    };

    let to_leave = match DB.get_server_settings(guild_id.0 as i64) {
        Ok(mut server) => match server.voice_channel == connect_to.0 as i64 {
            true => {
                msg.reply("I'm already using this voice channel")?;
                return Ok(());
            },
            false => {
                let old_voice_channel = server.voice_channel;
                server.voice_channel = connect_to.0 as i64;
                match DB.update_server_settings(server) {
                    Ok(_) => (),
                    Err(error) => warn!("Unable to update server information: {}", error),
                }

                Some(old_voice_channel)
            }
        },
        Err(error) => {
            warn!("Database is not available: {}", error);
            None
        }
    };

    let manager_lock = ctx.data.lock().get::<VoiceManager>().cloned().unwrap();
    let mut manager = manager_lock.lock();

    match to_leave {
        Some(0) | None => (),
        _ => match manager.leave(guild_id) {
            Some(_) => (),
            None => warn!("There is no voice channel to leave"),
        }
    }

    match manager.join(guild_id, connect_to) {
        Some(_) => {
            ctx.data.lock().get::<crate::discord::key::PlayList>().unwrap().do_send(crate::discord::play_list::AddServer(guild_id.0));
            msg.reply(&format!("Joined {}", connect_to.mention()))?
        },
        None => msg.reply("Cannot join it :(")?,
    };

    let handler = match manager.get_mut(guild_id) {
        Some(handler) => handler,
        None => {
            warn!("Cannot get handle on voice channel");
            return Ok(());
        },
    };
    handler.deafen(true);

    Ok(())
}

pub const STOP_MUSIC_DESC: &'static str = "Tells bot to stop playing";
pub const STOP_MUSIC_USAGE: &'static str = "";
pub fn stop_music(ctx: &mut Context, msg: &Message, _: Args) -> Result<(), CommandError> {
    let guild = match msg.guild() {
        Some(guild) => guild,
        None => {
            msg.reply("Groups and DMs not supported")?;

            return Ok(());
        }
    };

    let guild_id = guild.read().id;

    let manager_lock = ctx.data.lock().get::<VoiceManager>().cloned().unwrap();

    match manager_lock.lock().get(guild_id).is_some() {
        true => {
            ctx.data.lock().get::<crate::discord::key::PlayList>().unwrap().do_send(crate::discord::play_list::StopCurrent(guild_id.0));
            msg.reply("Stopping current track")?
        },
        false => msg.reply("I'm not in voice channel")?,
    };

    Ok(())
}

pub const VOICE_OFF_DESC: &'static str = "Tells bot to leave the voice channel";
pub const VOICE_OFF_USAGE: &'static str = "";
pub fn voice_off(ctx: &mut Context, msg: &Message, _: Args) -> Result<(), CommandError> {
    let guild = match msg.guild() {
        Some(guild) => guild,
        None => {
            msg.reply("Groups and DMs not supported")?;

            return Ok(());
        }
    };

    let guild_id = guild.read().id;

    let manager_lock = ctx.data.lock().get::<VoiceManager>().cloned().unwrap();
    let mut manager = manager_lock.lock();

    match manager.get(guild_id).is_some() {
        true => {
            manager.remove(guild_id);
            ctx.data.lock().get::<crate::discord::key::PlayList>().unwrap().do_send(crate::discord::play_list::RemoveServer(guild_id.0));

            msg.reply("I've left voice channel")?
        },
        false => msg.reply("I'm not in voice channel already")?,
    };

    match DB.get_server_settings(guild_id.0 as i64) {
        Ok(mut server) => {
            server.voice_channel = 0;
            match DB.update_server_settings(server) {
                Ok(_) => (),
                Err(error) => warn!("Unable to update server information: {}", error),
            }
        },
        Err(error) => warn!("Database is not available: {}", error),
    };


    Ok(())
}

pub const HANGMAN_ADD_DESC: &'static str = "Adds new word for hangman game";
pub const HANGMAN_ADD_USAGE: &'static str = "";
pub fn hangman_add(_: &mut Context, msg: &Message, args: Args) -> Result<(), CommandError> {
    let word = args.full();

    if word.contains(constants::WHITE_SPACE) {
        msg.reply("Multiple words are not allowed")?;
        return Ok(());
   }

    match lang::is_eng(word) {
        true => (),
        false => {
            msg.reply("Only English words are allowed")?;
            return Ok(());
        }
    }

    let guild_id = match msg.guild_id {
        Some(guild_id) => guild_id.0,
        None => return Ok(()),
    };

    let mut server = match DB.get_server(guild_id as i64) {
        Ok(server) => server,
        Err(error) => {
            warn!("Unable to query server: {}" , error);
            msg.reply(constants::text::INTERNAL_ERROR)?;
            return Ok(());
        }
    };

    let word = word.to_lowercase();

    if server.hangman_words.contains(word.as_str()) {
        msg.reply("Word is already exists")?;
        return Ok(());
    }

    if server.hangman_words.len() == 0 {
        server.hangman_words = word;
    } else {
        server.hangman_words = format!("{},{}", server.hangman_words, word);
    }

    match DB.update_server(server) {
        Ok(_) => msg.reply("Word has been added"),
        Err(error) => {
            warn!("Unable to update word in DB: {}", error);
            msg.reply(constants::text::INTERNAL_ERROR)
        }
    }?;

    Ok(())
}

pub const HANGMAN_REMOVE_DESC: &'static str = "Removes word for hangman game";
pub const HANGMAN_REMOVE_USAGE: &'static str = "";
pub fn hangman_remove(_: &mut Context, msg: &Message, args: Args) -> Result<(), CommandError> {
    let word = args.full();

    if word.contains(constants::WHITE_SPACE) {
        msg.reply("Multiple words are not allowed")?;
        return Ok(());
    }

    match lang::is_eng(word) {
        true => (),
        false => {
            msg.reply("Only English words are allowed")?;
            return Ok(());
        }
    }

    let guild_id = match msg.guild_id {
        Some(guild_id) => guild_id.0,
        None => return Ok(()),
    };

    let mut server = match DB.get_server(guild_id as i64) {
        Ok(server) => server,
        Err(error) => {
            warn!("Unable to query server: {}" , error);
            msg.reply(constants::text::INTERNAL_ERROR)?;
            return Ok(());
        }
    };

    let word = word.to_lowercase();
    server.hangman_words = match remove_word(&server.hangman_words, &word) {
        Some(new_words) => new_words,
        None => {
            msg.reply("There is no such word already")?;
            return Ok(());
        }
    };

    match DB.update_server(server) {
        Ok(_) => msg.reply("Word has been removed"),
        Err(error) => {
            warn!("Unable to update word in DB: {}", error);
            msg.reply(constants::text::INTERNAL_ERROR)
        }
    }?;

    Ok(())
}

fn remove_word(text: &str, word: &str) -> Option<String> {
    if text.len() == 0 || word.len() == 0 {
        return None;
    }

    let mut iter = text.split(word);

    let first = iter.next().unwrap();
    let second = match iter.next() {
        Some(second) => second,
        None => return None,
    };

    Some(format!("{}{}", &first[..first.len()-1], second))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_remove_word() {
        const TEXT: &'static str = "lolka,word,again";
        const WORD: &'static str = "word";
        const EXPECTED_TEXT: &'static str = "lolka,again";

        let result = remove_word(TEXT, WORD).expect("To get result");
        assert_eq!(result, EXPECTED_TEXT);
    }

    #[test]
    fn should_not_remove_word() {
        const TEXT: &'static str = "lolka,word,again";
        const WORD: &'static str = "word1";

        let result = remove_word(TEXT, WORD);
        assert!(result.is_none());
    }

    #[test]
    fn should_not_remove_empty_word() {
        const TEXT: &'static str = "lolka,word,again";
        const WORD: &'static str = "";

        let result = remove_word(TEXT, WORD);
        assert!(result.is_none());
    }

    #[test]
    fn should_not_remove_word_from_empty() {
        const TEXT: &'static str = "";
        const WORD: &'static str = "word";

        let result = remove_word(TEXT, WORD);
        assert!(result.is_none());
    }

}
