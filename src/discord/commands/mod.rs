const TIME_FORMAT: &'static str = "%H:%M:%S";
const JST_OFFSET: i32 = 9 * 3600; //9h

use constants;

fn default_embed(embed: serenity::builder::CreateEmbed) -> serenity::builder::CreateEmbed {
    embed.colour(super::BOT_COLOUR)
}

pub fn generate_welcome(channel_id: u64, name: &str, mention: String) -> serenity::Result<serenity::model::channel::Message> {
    let channel_id = serenity::model::id::ChannelId(channel_id);
    let welcome = constants::assets::create_welcome_image(name);

    let mut image_buffer = Vec::new();
    match welcome.write_to(&mut image_buffer, image::ImageOutputFormat::PNG) {
        Ok(_) => (),
        Err(error) => {
            warn!("Unable to write image into buffer");
            let error = std::io::Error::new(std::io::ErrorKind::Other, error);
            return Err(serenity::Error::Io(error));
        }
    }

    let image_buffer = image_buffer.as_slice();
    let attach = vec![(image_buffer, "welcome.png")];

    channel_id.send_files(attach, |msg| {
        msg.content(mention)
    })
}

pub mod general;
pub mod mods;
