use serenity::model::channel::Message;
use serenity::framework::standard::{Args, CommandError, ArgError};
use serenity::prelude::{Context};
use futures::Future;
use cute_dnd_dice::Roll;

use constants::{HANGMAN_PRICE, YOUTUBE_PRICE};
use discord::key::VoiceManager;
use db::DB;
use super::{default_embed, JST_OFFSET, TIME_FORMAT};
use random;
use constants;
use level::Level;

pub const PING_DESC: &'static str = "Check if the bot is alive.";
pub const PING_USAGE: &'static str = "";
pub fn ping(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    message.reply("pong")?;

    Ok(())
}

pub const PROFILE_DESC: &'static str = "Asks for user information.";
pub const PROFILE_USAGE: &'static str = "";
pub fn profile(_context: &mut Context, message: &Message, _: Args) -> Result<(), CommandError> {
    let user = match DB.get_or_new_user(message.author.id.0 as i64) {
        Ok(user) => user,
        Err(error) => {
            warn!("Unable to get user, error: {}", error);
            message.reply(constants::text::PROFILE_FAILED_TO_GET)?;
            return Ok(());
        }
    };
    let avatar = message.author.face();
    let level = Level::new(user.experience as u32);

    message.channel_id.send_message(|msg| {
        msg.embed(|msg| default_embed(msg).title(format_args!("{}'s profile", message.author.name))
                                          .thumbnail(avatar)
                                          .field("Level", level.level, false)
                                          .field("Yen", format_args!("{}¥", user.yen), true)
                                          .field("Experience", level, true)
        )
    })?;

    Ok(())
}

pub const ROLL_DESC: &'static str = "Rolls DnD dice";
pub const ROLL_USAGE: &'static str = "Example: 2d20+2, d4-1";
pub fn roll(_context: &mut Context, message: &Message, args: Args) -> Result<(), CommandError> {
    let roll = args.full_quoted();
    match Roll::from_str(roll) {
        Ok(roll) => message.reply(&format!("You roll {}", roll.roll()))?,
        Err(error) => message.reply(&format!("Cannot parse your roll: {}. Try better", error))?,
    };

    Ok(())
}

pub const JUDGE_DESC: &'static str = "Randomly choose someone to declare guilty.";
pub const JUDGE_USAGE: &'static str = "<One>, <Two>,... <N>";
pub fn judge(_context: &mut Context, message: &Message, mut args: Args) -> Result<(), CommandError> {
    for _ in 0..random::range(0, args.len()) {
        args.next();
    }
    let name = args.current().unwrap_or("");

    if name.starts_with("<@") {
        let name = serenity::utils::content_safe(name, &serenity::utils::ContentSafeOptions::new().clean_user(false).clean_channel(true).show_discriminator(false));
        message.reply(&format!("The criminal is {}", name))?;
    } else {
        message.reply(&format!("The criminal is `{}`", args.current().unwrap_or("")))?;
    }

    Ok(())
}

pub const TIME_DESC: &'static str = "Checks bot's time";
pub const TIME_USAGE: &'static str = "";
pub fn time(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    let now = chrono::Utc::now();
    let utc_time = now.time();
    let jst_time = now.with_timezone(&chrono::offset::FixedOffset::east(JST_OFFSET)).time();

    let text = format!("__UTC__\n**{}**\n__JST__\n**{}**\n", utc_time.format(TIME_FORMAT), jst_time.format(TIME_FORMAT));

    message.channel_id.send_message(|msg| {
        msg.embed(|msg| default_embed(msg).title("Time").description(text))
    })?;

    Ok(())
}

pub const DOSSIER_DESC: &'static str = "Provides useful information";
pub const DOSSIER_USAGE: &'static str = "";
const DOSSIER_TEXT: &'static str = "

__**Links:**__
--------------------------------------------
__Discord:__

[Invite](https://discord.gg/6exbetN)

--------------------------------------------
__Bloody Chronicles:__

[Website](http://www.bloodychronicles.com/)

--------------------------------------------
__Igrasil:__

[Twitter](https://twitter.com/Igrasilstudio)
[Website](http://www.igrasilstudio.com/)

--------------------------------------------
";

pub fn dossier(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    message.channel_id.send_message(|msg| {
        msg.embed(|msg| default_embed(msg).title("Dossier").description(DOSSIER_TEXT))
    })?;

    Ok(())
}

pub const WALLET_DESC: &'static str = "Checks your wallet.";
pub const WALLET_USAGE: &'static str = "";
pub fn wallet(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    match DB.get_or_new_user(message.author.id.0 as i64) {
        Ok(user) => {
            message.reply(&format!("Your wallet contains {}¥", user.yen))?;
            Ok(())
        },
        Err(error) => {
            warn!("Unable to get user, error: {}", error);
            Err(CommandError("You lost your wallet :(".to_owned()))
        }
    }
}

pub const GAMBLE_DESC: &'static str = "Try to win some money.";
pub const GAMBLE_USAGE: &'static str = "";
pub fn gamble(_context: &mut Context, message: &Message, _args: Args) -> Result<(), CommandError> {
    match DB.get_or_new_user(message.author.id.0 as i64) {
        Ok(mut user) => {
            let plus: i8 = random::range_inclusive(1, 15);
            let minus: i8 = random::range(1, 5);
            let result = plus - minus;

            if result > 0 {
                message.reply(&format!("You won {}¥", result))?;
            } else if result < 0 {
                message.reply(&format!("You lost {}¥", result))?;
            } else {
                message.reply(&format!("You couldn't win anythig"))?;
            }

            user.yen = user.yen + result as i64;
            //Let's be kind at least, and do not let user to get debts
            if user.yen < 0 {
                user.yen = 0;
            }

            match DB.update_user(user) {
                Ok(_) => (),
                Err(error) => warn!("Unable to update user's wallet. Error: {}", error),
            }

            Ok(())
        },
        Err(error) => {
            warn!("Unable to get user, error: {}", error);
            Err(CommandError("You lost your wallet :(".to_owned()))
        }
    }
}

pub const PLAY_DESC: &'static str = "Plays some song from youtube.";
pub const PLAY_USAGE: &'static str = "";
pub fn play(ctx: &mut Context, msg: &Message, args: Args) -> Result<(), CommandError> {
    let link = args.full_quoted().trim();

    if !(link.starts_with("http") && link.contains("youtu")) {
        msg.reply(constants::text::PLAY_NOT_YOUTUBE)?;
        return Ok(());
    }

    let mut user = match DB.get_or_new_user(msg.author.id.0 as i64) {
        Ok(user) => match user.yen < YOUTUBE_PRICE {
            true => {
                msg.reply(&format!("You cannot pay for it, song costs {}¥", YOUTUBE_PRICE))?;
                return Ok(());
            },
            false => user,
        },
        Err(error) => {
            warn!("Unable to get user, error: {}", error);
            msg.reply("You lost your wallet :(")?;
            return Ok(());
        },
    };

    let guild = match msg.guild() {
        Some(guild) => guild,
        None => {
            msg.reply("Groups and DMs not supported")?;

            return Ok(());
        }
    };
    let guild_id = guild.read().id;

    let audio = match serenity::voice::ytdl(link) {
        Ok(audio) => audio,
        Err(error) => {
            warn!("Cannot get audio from youtube link: {}", error);
            msg.reply(constants::text::PLAY_FAILED_TO_PLAY)?;
            return Ok(())
        }
    };

    let manager_lock = ctx.data.lock().get::<VoiceManager>().cloned().expect("Get VoiceManager");

    match manager_lock.lock().get(guild_id).is_some() {
        true => {
            ctx.data.lock().get::<crate::discord::key::PlayList>().expect("Get PlayList").do_send(crate::discord::play_list::AddSong(guild_id.0, audio));
            msg.reply("Adding Song to PlayList")?;
        },
        false => {
            msg.reply("I'm not in voice channel")?;
            return Ok(());
        }
    };

    user.yen = user.yen - YOUTUBE_PRICE;
    match DB.update_user(user) {
        Ok(_) => (),
        Err(error) => warn!("Unable to update user's wallet. Error: {}", error),
    }

    Ok(())
}

enum HangmanCommands {
    Play,
    List,
    Guess,
    Help,
    Unknown,
}

impl<'a> From<&'a str> for HangmanCommands {
    fn from(text: &'a str) -> Self {
        match text {
            "list" => HangmanCommands::List,
            "guess" => HangmanCommands::Guess,
            "play" => HangmanCommands::Play,
            "help" => HangmanCommands::Help,
            _ => HangmanCommands::Unknown,
        }
    }
}

pub const HANGMAN_DESC: &'static str = "Hangman game";
pub const HANGMAN_USAGE: &'static str = "play|list|guess";
pub fn hangman(ctx: &mut Context, msg: &Message, mut args: Args) -> Result<(), CommandError> {
    let guild_id = match msg.guild_id {
        Some(guild_id) => guild_id.0,
        None => return Ok(()),
    };

    let cmd: HangmanCommands = args.current().expect("First argument for hangman").into();

    match cmd {
        HangmanCommands::Guess => {
            use crate::discord::hangman::GuessResult;

            args.next();
            let letter = match args.single_quoted::<char>() {
                Ok(num) => num,
                Err(ArgError::Eos) => {
                    msg.reply(constants::text::HANGMAN_GAME_GUESS_MISSING_LETTER)?;
                    return Ok(());
                }
                Err(_) => {
                    msg.reply(constants::text::HANGMAN_GAME_GUESS_BAD_LETTER)?;
                    return Ok(());
                }
            };

            let res = ctx.data.lock().get::<crate::discord::key::Hangman>()
                                     .expect("Get Hangman")
                                     .send(crate::discord::hangman::GuessLetter(guild_id, letter));

            let res = match res.wait().expect("To have game manager available") {
                Some(res) => res,
                None => {
                    msg.reply(constants::text::HANGMAN_GAME_NOT_STARTED)?;
                    return Ok(());
                }
            };

            match res {
                GuessResult::Mada(current, left, is_guess) => match is_guess {
                    true => msg.reply(&format!("You guessed letter!\n`{}`\nYou has {} attempts left", current, left))?,
                    false => msg.reply(&format!("You were unable to guess letter!\n`{}`\nYou has {} attempts left", current, left))?,
                },
                GuessResult::Victory(word) => msg.reply(&format!("You win! The word is `{}`", word))?,
                GuessResult::GameOver(current) => msg.reply(&format!("You lost! You couldn't guess word in time. Remaining word `{}`", current))?,
            }
        },
        HangmanCommands::List => match DB.get_server_hangman_words(guild_id as i64) {
            Ok(words) => match words.len() {
                0 => msg.reply(constants::text::HANGMAN_LIST_NO_WORDS)?,
                _ => msg.reply(&format!("List of words: {}", words))?,
            },
            Err(error) => {
                warn!("Unable to query list of hangman words: {}", error);
                msg.reply(constants::text::INTERNAL_ERROR)?
            },
        },
        HangmanCommands::Help => {
            args.next();
            match args.current().unwrap_or("") {
                "play" => msg.reply(constants::text::HANGMAN_PLAY_HELP)?,
                "list" => msg.reply(constants::text::HANGMAN_LIST_HELP)?,
                "guess" => msg.reply(constants::text::HANGMAN_GUESS_HELP)?,
                "" => msg.reply(constants::text::HANGMAN_HELP)?,
                _ => msg.reply(constants::text::HANGMAN_UNKNOWN_HELP)?
            }
        },
        HangmanCommands::Play => {
            args.next();
            let attempts = match args.single_quoted::<usize>() {
                Ok(num) if num > 5 => num,
                Ok(_) => {
                    msg.reply(&format!("{}\nTry something more than 5 at least", constants::text::ERROR_NUMBER_TOO_LITTLE))?;
                    return Ok(());
                },
                Err(ArgError::Eos) => constants::hangman::DEFAULT_ATTEMPT_NUM,
                Err(_) => {
                    msg.reply(&format!("Cannot understand number of attempts. {}", constants::text::ERROR_NUMBER_PARSE))?;
                    return Ok(())
                }
            };

            let word = match DB.get_server_hangman_words(guild_id as i64) {
                Ok(word) => {
                    if word.len() == 0 {
                        msg.reply(constants::text::HANGMAN_GAME_NO_WORD)?;
                        return Ok(())
                    }

                    let words = word.split(',').collect::<Vec<_>>();
                    words[random::range(0, words.len())].to_owned()
                }
                Err(error) => {
                    warn!("Unable to query list of hangman words: {}", error);
                    msg.reply(constants::text::INTERNAL_ERROR)?;
                    return Ok(());
                }
            };

            let mut user = match DB.get_or_new_user(msg.author.id.0 as i64) {
                Ok(user) => match user.yen < HANGMAN_PRICE {
                    true => {
                        msg.reply(&format!("You cannot pay for it, hangman game costs {}¥", HANGMAN_PRICE))?;
                        return Ok(());
                    },
                    false => user,
                },
                Err(error) => {
                    warn!("Unable to get user, error: {}", error);
                    msg.reply("You lost your wallet :(")?;
                    return Ok(());
                },
            };

            let res = ctx.data.lock().get::<crate::discord::key::Hangman>()
                                     .expect("Get Hangman")
                                     .send(crate::discord::hangman::NewGame(guild_id, word, attempts));

            match res.wait().expect("To have game manager available") {
                Some(word) => {
                    user.yen = user.yen - HANGMAN_PRICE;
                    match DB.update_user(user) {
                        Ok(_) => (),
                        Err(error) => warn!("Unable to update user's wallet. Error: {}", error),
                    }

                    msg.reply(&format!("{}\nCurrent word: `{}`", constants::text::HANGMAN_GAME_STARTS, word))?
                },
                None => msg.reply(constants::text::HANGMAN_GAME_ALREADY)?,
            }
        },
        HangmanCommands::Unknown => return Ok(()),
    };

    Ok(())
}
