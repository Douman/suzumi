use std::sync;
use std::collections::HashSet;

use serenity::prelude::{Mentionable, RwLock, Mutex};
use serenity::client::{Client, EventHandler};
use serenity::client::bridge::voice::ClientVoiceManager;
use serenity::framework::standard::{Args, StandardFramework, CommandOptions, DispatchError};

use db::DB;
use constants;
use level::{self, Level};

const BOT_COLOUR: serenity::utils::Colour = serenity::utils::Colour::DARK_MAGENTA;
const TOKEN: &'static str = include_str!("../../DISCORD.token");

pub mod key {
    use super::*;

    pub struct VoiceManager;

    impl typemap::Key for VoiceManager {
        type Value = sync::Arc<Mutex<ClientVoiceManager>>;
    }

    pub struct PlayList;

    impl typemap::Key for PlayList {
        type Value = actix::Addr<play_list::Manager>;
    }

    pub struct Hangman;

    impl typemap::Key for Hangman {
        type Value = actix::Addr<hangman::Manager>;
    }

    pub struct MyId;

    impl typemap::Key for MyId {
        type Value = u64;
    }
}

pub mod play_list;
pub mod hangman;
mod commands;

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, _: serenity::prelude::Context, _bot_data: serenity::model::gateway::Ready) {
        info!("Discord: Serenity connected");
    }

    fn resume(&self, _: serenity::prelude::Context, _: serenity::model::event::ResumedEvent) {
        info!("Discord: Serenity reconnected");
    }

    fn message(&self, _ctx: serenity::prelude::Context, msg: serenity::model::channel::Message) {
        if msg.guild_id.is_none() || msg.author.bot == true || msg.content.starts_with(constants::CMD_PREFIX) {
            return;
        }

        let exp = match constants::message_exp_points(&msg) {
            0 => return,
            exp => exp
        };

        let user_id = msg.author.id.0 as i64;

        match DB.get_or_new_user(user_id) {
            Ok(mut user) => {
                let mut level = Level::new(user.experience as u32);
                match level.add(exp as u32) {
                    level::AddResult::Maxed => return,
                    level::AddResult::Added => (),
                    level::AddResult::LevelUp => {
                        let _ = msg.reply(&format!("Congratulations on level up! Your new level is {}", level.level));
                    }
                }

                user.experience = level.exp as i64;

                match DB.update_user(user) {
                    Ok(_) => (),
                    Err(error) => warn!("Unable to add experience for user. Error: {}", error),
                }
            },
            Err(error) => warn!("Unable to get user. Db error: {}", error)
        }
    }

    fn cached(&self, ctx: serenity::prelude::Context, guilds: Vec<serenity::model::id::GuildId>) {
        for id in guilds.iter().map(|guild| guild.0) {
            match DB.new_server(id as i64) {
                Ok(true) => debug!("Added new server {}", id),
                Ok(false) => {
                    debug!("Server already added");
                    match DB.get_server_settings(id as i64) {
                        Ok(server) => {
                            let connect_to = server.voice_channel as u64;

                            if connect_to != 0 {
                                //Rejoin voice channel if any
                                let manager_lock = ctx.data.lock().get::<key::VoiceManager>().cloned().expect("Get VoiceManager");
                                let mut manager = manager_lock.lock();

                                match manager.join(id, connect_to) {
                                    Some(_) => {
                                        ctx.data.lock().get::<key::PlayList>().expect("Get PlayList").do_send(play_list::AddServer(id));
                                    },
                                    None => warn!("Couldn't re-join old voice channel"),
                                }
                                match manager.get_mut(id) {
                                    Some(handler) => handler.deafen(true),
                                    None => warn!("Cannot get handle on voice channel"),
                                }
                            }
                        },
                        Err(error) => warn!("Cannot get existing server: {}", error),
                    }
                },
                Err(error) => warn!("Failed to add server: {}", error),
            }
        }
    }

    fn guild_create(&self, _: serenity::prelude::Context, guild: serenity::model::guild::Guild, is_new: bool) {
        if !is_new {
            return;
        }

        let id = guild.id.0 as i64;
        match DB.new_server(id) {
            Ok(true) => debug!("Added new server {}", id),
            Ok(false) => debug!("Server already added"),
            Err(error) => warn!("Failed to add server: {}", error),
        }
    }

    fn guild_delete(&self, _: serenity::prelude::Context, partial_guild: serenity::model::guild::PartialGuild, _: Option<sync::Arc<RwLock<serenity::model::guild::Guild>>>) {
        match DB.remove_server(partial_guild.id.0 as i64) {
            Ok(_) => debug!("Server has been removed"),
            Err(error) => warn!("Failed to remove server: {}", error),
        }
    }

    fn guild_member_addition(&self, _: serenity::prelude::Context, guild: serenity::model::id::GuildId, user: serenity::model::guild::Member) {
        let user_id = {
            let user = user.user.read();

            if user.bot {
                return;
            }

            user.id.0 as i64
        };

        let server_id = guild.0 as i64;

        match DB.get_or_new_user(user_id) {
            Ok(_) => match DB.get_server_settings(server_id) {
                Ok(server) => {
                    if server.welcome_channel > 0 {
                        let (name, mention) = {
                            let user = user.user.read();
                            (user.name.clone(), user.id.mention())
                        };
                        match commands::generate_welcome(server.welcome_channel as u64, name.as_str(), mention) {
                            Ok(_) => (),
                            Err(error) => warn!("Welcome error: {}", error),
                        }
                    }
                },
                Err(_) => warn!("No server information"),
            },
            Err(error) => warn!("Unable to get new user. Db error: {}", error)
        }
    }

    fn voice_state_update(&self, ctx: serenity::prelude::Context, guild: Option<serenity::model::id::GuildId>, voice: serenity::model::voice::VoiceState) {
        let my_id = ctx.data.lock().get::<key::MyId>().cloned().unwrap_or(0);

        if my_id == 0 {
            return;
        }

        let guild = match guild {
            Some(guild) => guild,
            None => return,
        };

        if my_id != voice.user_id.0 {
            return;
        }

        let new_voice_id = voice.channel_id.map(|id| id.0).unwrap_or(0) as i64;

        match DB.get_server_settings(guild.0 as i64) {
            Ok(mut server) => match server.voice_channel != new_voice_id {
                false => (),
                true => {
                    info!("I'm dragged out of current voice channel({}) :(", server.voice_channel);
                    if new_voice_id == 0 {
                        ctx.data.lock().get::<crate::discord::key::PlayList>().unwrap().do_send(crate::discord::play_list::RemoveServer(server.voice_channel as u64));
                    }

                    server.voice_channel = new_voice_id;
                    match DB.update_server_settings(server) {
                        Ok(_) => (),
                        Err(error) => warn!("Unable to update server information: {}", error),
                    }
                }
            },
            Err(error) => {
                warn!("Database is not available: {}", error);
                return;
            }
        }
    }
}

pub fn start(play_list_manager: actix::Addr<play_list::Manager>, hangman_manager: actix::Addr<hangman::Manager>) {
    use serenity::framework::standard::{HelpBehaviour, help_commands};

    let mut client = Client::new(TOKEN, Handler).expect("Error creating client");
    play_list_manager.do_send(play_list::SetVoiceManager(client.voice_manager.clone()));

    let (my_id, owners) = match serenity::http::get_current_application_info() {
        Ok(info) => {
            let mut set = HashSet::new();
            set.insert(info.owner.id);

            (info.id.0, set)
        }
        Err(error) => {
            error!("Couldn't get application info: {:?}", error);
            (0, HashSet::new())
        }
    };

    {
        let mut data = client.data.lock();
        data.insert::<key::VoiceManager>(client.voice_manager.clone());
        data.insert::<key::PlayList>(play_list_manager.clone());
        data.insert::<key::Hangman>(hangman_manager.clone());
        data.insert::<key::MyId>(my_id);
    }


    let framework = StandardFramework::new().simple_bucket("gamble", 600)
                                            .on_dispatch_error(on_dispatch_error)
                                            .configure(|c| c.prefix(constants::CMD_PREFIX)
                                                            .ignore_bots(true)
                                                            .case_insensitivity(true)
                                                            .allow_dm(true)
                                                            .owners(owners)
                                                            .delimiters(vec![",", " "])
                                            ).customised_help(help_commands::plain, |help| {
                                                help.individual_command_tip("If you want more information about a command pass it as an argument to help.")
                                                    .lacking_role(HelpBehaviour::Hide)
                                                    .wrong_channel(HelpBehaviour::Hide)
                                                    .lacking_permissions(HelpBehaviour::Hide)
                                                    //.embed_success_colour(BOT_COLOUR)
                                                    //.embed_error_colour(BOT_COLOUR)
                                                    .striked_commands_tip(None)
                                            }).group("For Everyone", |group| {
                                                group.help_available(true)
                                                     .command("ping", |config| {
                                                         config.desc(commands::general::PING_DESC)
                                                               .usage(commands::general::PING_USAGE)
                                                               .exec(commands::general::ping)
                                                     }).command("profile", |config| {
                                                         config.desc(commands::general::PROFILE_DESC)
                                                               .usage(commands::general::PROFILE_USAGE)
                                                               .exec(commands::general::profile)
                                                     }).command("roll", |config| {
                                                         config.desc(commands::general::ROLL_DESC)
                                                               .usage(commands::general::ROLL_USAGE)
                                                               .exec(commands::general::roll)
                                                               .min_args(1)
                                                     }).command("judge", |config| {
                                                         config.desc(commands::general::JUDGE_DESC)
                                                               .usage(commands::general::JUDGE_USAGE)
                                                               .min_args(2)
                                                               .help_available(true)
                                                               .exec(commands::general::judge)
                                                     }).command("time", |config| {
                                                         config.desc(commands::general::TIME_DESC)
                                                               .usage(commands::general::TIME_USAGE)
                                                               .exec(commands::general::time)
                                                     }).command("dossier", |config| {
                                                         config.desc(commands::general::DOSSIER_DESC)
                                                               .usage(commands::general::DOSSIER_USAGE)
                                                               .exec(commands::general::dossier)
                                                     }).command("wallet", |config| {
                                                         config.desc(commands::general::WALLET_DESC)
                                                               .usage(commands::general::WALLET_USAGE)
                                                               .exec(commands::general::wallet)
                                                     }).command("hangman", |config| {
                                                         config.desc(commands::general::HANGMAN_DESC)
                                                               .usage(commands::general::HANGMAN_USAGE)
                                                               .exec(commands::general::hangman)
                                                               .min_args(1)
                                                     }).command("gamble", |config| {
                                                         config.desc(commands::general::GAMBLE_DESC)
                                                               .usage(commands::general::GAMBLE_USAGE)
                                                               .bucket("gamble")
                                                               .exec(commands::general::gamble)
                                                     })
                                            }).group("Music player", |group| {
                                                group.help_available(true)
                                                     .guild_only(true)
                                                     .command("play", |config| {
                                                         config.desc(commands::general::PLAY_DESC)
                                                               .usage(commands::general::PLAY_USAGE)
                                                               .exec(commands::general::play)
                                                               .min_args(1)
                                                               .max_args(1)
                                                     })
                                            }).group("For Moderators", |group| {
                                                group.help_available(true)
                                                     .guild_only(true)
                                                     .check(mod_check)
                                                     .command("set_welcome_channel", |config| {
                                                         config.desc(commands::mods::SET_WELCOME_DESC)
                                                               .usage(commands::mods::SET_WELCOME_USAGE)
                                                               .exec(commands::mods::set_welcome_channel)
                                                     }).command("voice_on", |config| {
                                                         config.desc(commands::mods::VOICE_ON_DESC)
                                                               .usage(commands::mods::VOICE_ON_USAGE)
                                                               .exec(commands::mods::voice_on)
                                                     }).command("voice_off", |config| {
                                                         config.desc(commands::mods::VOICE_OFF_DESC)
                                                               .usage(commands::mods::VOICE_OFF_USAGE)
                                                               .exec(commands::mods::voice_off)
                                                     }).command("hangman_add", |config| {
                                                         config.desc(commands::mods::HANGMAN_ADD_DESC)
                                                               .usage(commands::mods::HANGMAN_ADD_USAGE)
                                                               .exec(commands::mods::hangman_add)
                                                               .min_args(1)
                                                               .max_args(1)
                                                     }).command("hangman_remove", |config| {
                                                         config.desc(commands::mods::HANGMAN_REMOVE_DESC)
                                                               .usage(commands::mods::HANGMAN_REMOVE_USAGE)
                                                               .exec(commands::mods::hangman_remove)
                                                               .min_args(1)
                                                               .max_args(1)
                                                     }).command("stop_music", |config| {
                                                         config.desc(commands::mods::STOP_MUSIC_DESC)
                                                               .usage(commands::mods::STOP_MUSIC_USAGE)
                                                               .exec(commands::mods::stop_music)
                                                     })
                                            });

    client.with_framework(framework);

    match client.start_autosharded() {
        Ok(_) => (),
        Err(error) => warn!("Discord exited with error: {}", error),
    }

    play_list_manager.do_send(play_list::UnsetVoiceManager);
}

fn on_dispatch_error(_: serenity::prelude::Context, message: serenity::model::channel::Message, error: DispatchError) {
    let res = match error {
        DispatchError::RateLimited(remaining) => {
            let minutes = remaining / 60;
            let seconds = remaining % 60;

            match minutes {
                0 => message.reply(&format!("Please wait just a bit more. Remains {} seconds", remaining)),
                _ => message.reply(&format!("You shouldn't do it so much. Wait {}:{} minutes", minutes, seconds)),
            }
        },
        DispatchError::NotEnoughArguments {min, given} => message.reply(&format!("Command needs at least {} arguments, {} were given", min, given)),
        DispatchError::TooManyArguments {max, given} => message.reply(&format!("Command needs no more than {} arguments, {} were given", max, given)),
        DispatchError::CheckFailed => message.reply("Command is available for moderator only"),
        _ => return,
    };

    match res {
        Err(error) => warn!("Failed to handle error: {}", error),
        _ => (),
    }
}

fn mod_check(_ctx: &mut serenity::prelude::Context, message: &serenity::model::channel::Message, _args: &mut Args, _options: &CommandOptions) -> bool {
    if let Some(guild) = message.guild() {
        let (guild_id, owner_id) = {
            let guild = guild.read();
            (guild.id, guild.owner_id)
        };

        if message.author.id == owner_id {
            return true;
        }

        if let Ok(member) = guild_id.member(message.author.id.clone()) {
            for role in member.roles.iter().map(|id| id.to_role_cached()).filter(|id| id.is_some()).map(|role| role.unwrap()) {
                match role.name.as_str() {
                    "Moderator" => return true,
                    _ => ()
                }
            }
        }
    }

    false
}
