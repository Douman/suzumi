#![allow(proc_macro_derive_resolution_fallback)]

extern crate serenity;
extern crate typemap;
extern crate cute_log;
#[macro_use]
extern crate log;
extern crate rand;
extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate lazy_static;
extern crate actix;
extern crate whatlang;
extern crate futures;
extern crate image;
extern crate rusttype;
extern crate cute_dnd_dice;

mod lang;
mod constants;
mod level;
mod command;
mod random;
mod db;
mod rt;
mod discord;

use actix::{Actor};

fn run_system() {
    let system = actix::System::new("Suzumi".to_owned());

    let play_list = discord::play_list::Manager::default().start();
    let hangman_manager = discord::hangman::Manager::default().start();

    std::thread::Builder::new().name("discord-worker".to_owned()).spawn(move || {
        loop {
            discord::start(play_list.clone(), hangman_manager.clone());
            std::thread::sleep(std::time::Duration::from_secs(10));
        }
    }).expect("To create discord-worker thread");

    match system.run() {
        Ok(_) => (),
        Err(error) => warn!("System finished with error={}", error),
    }
}

fn main() {
    let _ = cute_log::init();
    rt::init();

    if !command::verify_availability("youtube-dl") {
        warn!("youtube-dl is not available for use. Will not be able to play youtube");
    }
    if !command::verify_availability("ffmpeg") {
        warn!("ffmpeg is not available for use. Will not be able to play any audio");
    }

    run_system();
}
