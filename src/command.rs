use std::process::{self, Command};

///Verifies if command available to use
pub fn verify_availability(name: &str) -> bool {
    Command::new(name).arg("-h")
                      .stdout(process::Stdio::null())
                      .stderr(process::Stdio::null())
                      .status()
                      .map(|status| status.success())
                      .unwrap_or(false)
}
