//Formula 5 * (lvl ^ 2) + 50 * lvl + 100
use std::cmp;
use std::fmt;

const MAX_LEVEL: u8 = 10;
const MAX_EXP: u32 = 55 * MAX_LEVEL as u32 * MAX_LEVEL as u32 * MAX_LEVEL as u32;

#[derive(Debug, PartialEq)]
pub enum AddResult {
    Maxed,
    LevelUp,
    Added,
}

pub struct Level {
    pub exp: u32,
    pub level: u8,
}

impl Level {
    pub fn new(exp: u32) -> Self {
        let level = Self::exp_to_level(exp);

        Self {
            exp,
            level,
        }
    }

    ///Adds experience points and returns
    ///whether level up happened.
    pub fn add(&mut self, val: u32) -> AddResult {
        if self.level >= MAX_LEVEL {
            return AddResult::Maxed;
        }

        self.exp = cmp::min(MAX_EXP, self.exp + val);
        let new_level = Self::exp_to_level(self.exp);

        if new_level != self.level {
            self.level = new_level;
            AddResult::LevelUp
        } else {
            AddResult::Added
        }
    }

    fn exp_to_level(exp: u32) -> u8 {
        let exp = cmp::min(exp, MAX_EXP);

        let exp = (exp / 55) as f32;
        let exp = exp.powf(1.0 / 3.0);
        exp as u8
    }
}

impl fmt::Display for Level {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        match self.level == MAX_LEVEL {
            true => write!(w, "{}", MAX_EXP),
            false => {
                let next_level = self.level as u32 + 1;
                let exp_until_next = 55 * next_level * next_level * next_level;
                write!(w, "{}/{}", self.exp, exp_until_next)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_level_up() {
        let mut level = Level::new(550);

        assert_eq!(level.add(0), AddResult::Added);
        assert_eq!(level.level, 2);
        assert_eq!(level.add(1), AddResult::Added);
        assert_eq!(level.level, 2);
        assert_eq!(level.add(10), AddResult::Added);
        assert_eq!(level.level, 2);
        let add_exp = 1_485 - level.exp - 1;
        assert_eq!(level.add(add_exp), AddResult::Added);
        assert_eq!(level.level, 2);
        assert_eq!(level.add(1), AddResult::LevelUp);
        assert_eq!(level.exp, 1_485);
        println!("{}", level);
        assert_eq!(level.level, 3);
        assert_eq!(level.add(MAX_EXP), AddResult::LevelUp);
        assert_eq!(level.exp, MAX_EXP);
        assert_eq!(level.level, 10);
        println!("{}", level);
    }

    #[test]
    fn verify_level() {
        assert_eq!(Level::exp_to_level(MAX_EXP), 10);

        assert_eq!(Level::exp_to_level(55 * 5 * 5 * 5), 5);
        assert_eq!(Level::exp_to_level(55 * 5 * 5 * 5 - 1), 4);

        assert_eq!(Level::exp_to_level(0), 0);
        assert_eq!(Level::exp_to_level(50), 0);

        assert_eq!(Level::exp_to_level(550), 2);
    }
}

