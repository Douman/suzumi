//!Describes constants

///Price to pay for youtube playlist
pub const YOUTUBE_PRICE: i64 = 10;
pub const HANGMAN_PRICE: i64 = 1;

pub const CMD_PREFIX: &'static str = "!";
pub const WHITE_SPACE: &'static [char] = &[' ', '\t'];

///Calculates experience points based on serenity's Message
pub fn message_exp_points(msg: &serenity::model::channel::Message) -> i8 {
    //Other messages are not regular chatting
    if msg.kind != serenity::model::channel::MessageType::Regular {
        return 0;
    }

    let len = msg.content.chars().count();

    if len < 10 {
        0
    } else if len < 25 {
        1
    } else if len < 50 {
        2
    } else if len < 100 {
        4
    } else {
        6
    }
}

pub mod assets {
    ///Creates Welcome Image for user by name.
    ///
    ///The calculations are font-dependent and pretty crude
    pub fn create_welcome_image(name: &str) -> image::DynamicImage {
        const LETTER_SIZE: f32 = 28.0;
        const TEXT_BOX_X_MAX: u32 = 900;
        const TEXT_BOX_X: u32 = 460;
        //Modify Y position accordingly to font
        const TEXT_BOX_Y: u32 = 364;

        //~18px per letter so we need to crop a bit
        //Discord allows up to 32 letters for nickname
        //but smaller font doesn't look so good
        //To fit 32 we'd need ~14px letter
        //TODO: it works well for latin letters
        //but current font may not support all of UTF-8
        const LETTER_PX_X: u32 = 18;

        const TEXT_BOX_X_SIZE: u32 = TEXT_BOX_X_MAX - TEXT_BOX_X;
        const MAX_LETTER_NUM: usize = TEXT_BOX_X_SIZE as usize / LETTER_PX_X as usize;

        let name = name.trim_matches(|ch| FONT.glyph(ch).id().0 == 0);
        let name = name.replace(|ch| FONT.glyph(ch).id().0 == 0, " ");
        let name = name.as_str();

        let mut name_len = 0;
        //Japanese/Chinese full width may take 4 bytes
        //while actual length of character would take space
        //of two latin characters
        for ch in name.chars() {
            name_len += std::cmp::min(ch.len_utf8(), 2);
        }

        let name = if name_len > MAX_LETTER_NUM {
            //Safeguard against hitting middle of Unicode character
            let mut max = MAX_LETTER_NUM;
            let mut result = name.get(..max);
            while result.is_none() {
                max -= 1;
                result = name.get(..max);
            }

            result.unwrap()
        } else {
            name
        };

        let name_len_px = std::cmp::min(TEXT_BOX_X_SIZE, name_len as u32 * LETTER_PX_X);
        let shift_x = (TEXT_BOX_X_SIZE - name_len_px) / 2;
        let scale = rusttype::Scale { x: LETTER_SIZE * 1.25, y: LETTER_SIZE * 1.25 };

        let mut welcome = image::load_from_memory_with_format(include_bytes!("../assets/images/welcome.png"), image::ImageFormat::PNG).expect("Load welcome");
        imageproc::drawing::draw_text_mut(&mut welcome, image::Rgba([238, 183, 149, 255]), TEXT_BOX_X + shift_x, TEXT_BOX_Y, scale, &FONT, name);

        welcome
    }

    lazy_static! {
        pub static ref FONT: rusttype::Font<'static> = rusttype::FontCollection::from_bytes(Vec::from(include_bytes!("../assets/fonts/welcome.ttf") as &[u8])).expect("Load font").into_font().unwrap();
    }
}

pub mod hangman {
    pub const DEFAULT_ATTEMPT_NUM: usize = 20;
}

pub mod text {
    pub const INTERNAL_ERROR: &'static str = "There are some problems... Please try later :(";
    pub const ERROR_NUMBER_PARSE: &'static str = "Not a valid number";
    pub const ERROR_NUMBER_TOO_LITTLE: &'static str = "It is really low number!";

    pub const PLAY_NOT_YOUTUBE: &'static str = "This is not a youtube link";
    pub const PLAY_FAILED_TO_PLAY: &'static str = "I cannot play it :(";

    pub const PROFILE_FAILED_TO_GET: &'static str = "I cannot find your information, I'm sorry :(";

    pub const HANGMAN_HELP: &'static str = "Available ones are: `list`, `play`, `guess`";
    pub const HANGMAN_UNKNOWN_HELP: &'static str = "Unknown command\nAvailable ones are: `list`, `play`, `guess`";
    pub const HANGMAN_LIST_HELP: &'static str = "Usage: list";
    pub const HANGMAN_LIST_NO_WORDS: &'static str = "There are no words yet";
    pub const HANGMAN_PLAY_HELP: &'static str = "Usage: play [number_of_attempts]\nDefault number of attempts is 20\nRandom word is chosen";

    pub const HANGMAN_GUESS_HELP: &'static str = "Usage: guess <letter>";
    pub const HANGMAN_GAME_NO_WORD: &'static str = "I have no words to chose from :( Ask moderators to add some";
    pub const HANGMAN_GAME_STARTS: &'static str = "Game starts!\nTry to uncover word before you run out of attempts!";
    pub const HANGMAN_GAME_ALREADY: &'static str = "There is already ongoing game! Finish it, before starting new one";
    pub const HANGMAN_GAME_GUESS_MISSING_LETTER: &'static str = "What letter you want to guess?";
    pub const HANGMAN_GAME_GUESS_BAD_LETTER: &'static str = "You can input only single letter";
    pub const HANGMAN_GAME_NOT_STARTED: &'static str = "Game hasn't been started yet";
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_font_nihingo() {
        let text = "おしざーおしざー";

        let welcome = assets::create_welcome_image(text);
        let mut image_buffer = Vec::new();
        welcome.write_to(&mut image_buffer, image::ImageOutputFormat::PNG).expect("Write buffer");
        std::fs::write("test1.png", image_buffer.as_slice()).expect("Write file");
    }

    #[test]
    fn verify_font_eigo() {
        let text = "Chitanda Eru";

        let welcome = assets::create_welcome_image(text);
        let mut image_buffer = Vec::new();
        welcome.write_to(&mut image_buffer, image::ImageOutputFormat::PNG).expect("Write buffer");
        std::fs::write("test2.png", image_buffer.as_slice()).expect("Write file");
    }

    #[test]
    fn verify_font_emoji() {
        let text = "✦ღGlitter Gal Lilacღ✦";

        let welcome = assets::create_welcome_image(text);
        let mut image_buffer = Vec::new();
        welcome.write_to(&mut image_buffer, image::ImageOutputFormat::PNG).expect("Write buffer");
        std::fs::write("test3.png", image_buffer.as_slice()).expect("Write file");
    }

}
