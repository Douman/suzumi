-- Your SQL goes here
CREATE TABLE IF NOT EXISTS servers (
    id BIGINT NOT NULL,
    welcome_channel BIGINT NOT NULL DEFAULT 0,
    voice_channel BIGINT NOT NULL DEFAULT 0,
    hangman_words TEXT NOT NULL DEFAULT '',
    PRIMARY KEY (id)
)
