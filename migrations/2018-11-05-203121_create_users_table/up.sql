-- Current assumption is single discord server
-- If needed you can make composite PRIMARY KEY out of user ID and server ID
CREATE TABLE IF NOT EXISTS users (
    id BIGINT NOT NULL,
    yen BIGINT NOT NULL DEFAULT 100,
    experience BIGINT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
)
