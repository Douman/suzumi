# Suzumi

Cute bot for Bloody Chronicles Discord.

## Requirements

### Build

Store token in repository root as file `DISCORD.token` without newlines

Install

```bash
apt-get install -y libsodium-dev libopus0 pkg-config
```

### Running

Host wiht bot should has available in `PATH` following tools:

- `ffmpeg`
- `youtube-dl`

## Database

- Sqlite
- Stored in `./suzumi.db`
